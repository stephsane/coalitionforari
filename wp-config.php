<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'cari' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Yd71-?B.W8erC!k)W=NqTsFW].ELG|NB|#cf&>)k9jb+JH7Y88!W0ayYz+A+FnyQ' );
define( 'SECURE_AUTH_KEY',  '+ex.(1,oVlNTG9InaVIl#OLWb<M7XYI2MRZWpy[]v3[bt^/]oQmf .A~yS13Ood^' );
define( 'LOGGED_IN_KEY',    '16qwW;s>7YOf[t12/KmR]X1m1k*lI:7yCjF#SSl//t5%Tq;Cy#=QhOpyx^tT)P8j' );
define( 'NONCE_KEY',        '>!lNwxgnW(.I22?tt/]y>W[nl`eYT}h*VFv<~Q[sX,[db}wK@>B1Grevx!_w @pW' );
define( 'AUTH_SALT',        'ET-~/zD<j:!Ko}O(ejN[QKQ.0kpE5*+m*B13km3Eec>{mauv/Kpu)soU$!6la4dO' );
define( 'SECURE_AUTH_SALT', 'G1-|C-z`KPaZ}a~*xHJ_ey<oAODSupZlcOaCyQnRSG}vvBe$n9/JjwplaLwAwogv' );
define( 'LOGGED_IN_SALT',   'j_i1#.1}yP&Ib-cqIbE>2h]8 ?As93N:xch*2d%tze5:hSSg@/J~lXMT&LYZtLSK' );
define( 'NONCE_SALT',       'o.HZ9#LFtfcX_(b;ATHn0&?e)(wUIBO?p1{4A`0_GRa%:UPIWw<_S?_qk!`/rGO]' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

//Change Memory Limit
define('WP_MEMORY_LIMIT', '256M');